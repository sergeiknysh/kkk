Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZDIwYzM1MC1kYWNmLTQyMWQtYWFlNi02YWY2MjFjZDZkZmMiLCJpZCI6ODc5MSwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU1MjgyMzI3N30.FVbhAcjUughhOY044nMiv9gYLXf8pQI7kFnvGtjNPGE';
/* var url='http://localhost:8080/geoserver/wms'; //Geoserver URL
var layers = widget.scene.globe.imageryLayers;
    layers.removeAll();
    layers.addImageryProvider(new Cesium.WebMapServiceImageryProvider({
        url : url,        
        layers: 'GIS_Demo'// Here just give layer name
    }));
 */
let viewer = new Cesium.Viewer('cesiumContainer', {
	scene3DOnly: true,
	selectionIndicator: false, // ?
	geocoder: false,
	homeButton: false,
	sceneModePicker: false,
	baseLayerPicker: false,
	navigationHelpButton: false,
	animation: false,
	timeline: false,
});

let camera = viewer.camera;
let scene = viewer.scene;
scene.globe.depthTestAgainstTerrain = false;
scene.primitives.removeAll();





// @note empirical correction
let hpr = new Cesium.HeadingPitchRoll(Cesium.Math.toRadians(91.8), 0, 0);
let speed = 10;

function createObj(id, name, uri, pos, hpr) {
	// return viewer.entities.add({
	// 	id: 'ded',
	// 	position: pos,
	// 	orientation: Cesium.Transforms.headingPitchRollQuaternion(pos, hpr),
	// 	model: {
	// 		uri: 'models/bron.glb',
	// 	}
	// });
	return scene.primitives.add(Cesium.Model.fromGltf({
		id: uri,
		url: uri,
		heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
		modelMatrix: Cesium.Transforms.headingPitchRollToFixedFrame(pos, hpr)
	}));
}


function showObj(camera_data, max_speed) {
	// let distance = calcDistance(camera.position, camera_data.destination);
	camera.flyTo(camera_data);
}

function calcDistance(point1, point2) {
	let surfacePositions = Cesium.PolylinePipeline.generateArc({
		positions: [point1, point2]
	});

	let scratchCartesian3 = new Cesium.Cartesian3();
	let surfacePositionsLength = surfacePositions.length;
	let totalDistanceInMeters = 0;
	for (var i = 3; i < surfacePositionsLength; i += 3) {
		scratchCartesian3.x = surfacePositions[i] - surfacePositions[i - 3];
		scratchCartesian3.y = surfacePositions[i + 1] - surfacePositions[i - 2];
		scratchCartesian3.z = surfacePositions[i + 2] - surfacePositions[i - 1];
		totalDistanceInMeters += Cesium.Cartesian3.magnitude(scratchCartesian3);
	}

	return totalDistanceInMeters;
}

// init models
let $object_views_select = $('#objectViews');
let $layers_views_check = $('#layersArray');

let models = {};
for (id in data) {
	let obj_data = data[id];

	if (obj_data.src) {
		let pos = Cesium.Cartesian3.fromDegrees(obj_data.lng, obj_data.lat);
		models[id] = createObj(id, obj_data.title, obj_data.src, pos, hpr);

	}

	$('<input type="checkbox" name="layers[]" checked value="' + obj_data.title + '" >' + obj_data.title + '</input>')
		.data('objId', obj_data.src)
		.appendTo($layers_views_check);
	$('<br/>').appendTo($layers_views_check);
}

for (id in cameraPos) {
	let obj_data = cameraPos[id];
	let obj_camera = obj_data.camera;
	if (obj_camera) {
		let camera_data_attr = {
			destination: Cesium.Cartesian3.fromDegrees(
				obj_camera.pos.lng,
				obj_camera.pos.lat,
				obj_camera.pos.h
			),
			orientation: {
				heading: obj_camera.h,
				pitch: obj_camera.p,
				roll: obj_camera.r,
			},
		};

		$('<option value="' + id + '">' + obj_data.title + '</option>')
			.data('camera', camera_data_attr)
			.appendTo($object_views_select);
	}
}

function getModelForEntity(entity) {
	//console.log(entity);
	var primitives = viewer.scene.primitives;
	for (var i = 0; i < primitives.length; i++) {
		var primitive = primitives.get(i);
		console.log(primitive.id);
		if (primitive instanceof Cesium.Model && primitive.id == entity) {
			return primitive;
		}
	}
};

/* var primitives = viewer.scene.primitives;
	for (var i = 0; i < primitives.length; i++) {
		var primitive = primitives.get(i);
		if (primitive instanceof Cesium.Model ) {
			console.log(primitive.id);
		}
	} 
 */

$("#layersArray :checkbox").change(function (e) {
	let entity = $(this).data('objId');
	if (entity) {
		/* console.log("Got entity!");
		console.log(entity); */
	}

	if ($(this).is(":checked")) {
		// console.log("checked : " + $(this).data('objId').id.name);
		getModelForEntity(entity).show = true;
		// console.log(getModelForEntity(entity));
	} else {
		// console.log(getModelForEntity(entity));
		getModelForEntity(entity).show = false;
		// console.log("not checked : " + $(this).data('objId').id.name);
	}
});

// init camera
$('#objectViews').on('change', function () {
	let camera_data = $(this.options[this.selectedIndex]).data('camera');
	showObj(camera_data);
}).trigger('change');

// dump camera
function dc() {
	let carto = Cesium.Cartographic.fromCartesian(camera.position);
	console.log(JSON.stringify({
		pos: {
			h: carto.height,
			lat: Cesium.Math.toDegrees(carto.latitude),
			lng: Cesium.Math.toDegrees(carto.longitude),
		},
		h: camera.heading,
		p: camera.pitch,
		r: camera.roll,
	}))
}

$(function () {
	$(document).on('keypress', function (e) {
		if (e.keyCode == 13) {
			dc();
		}
	})
});

var cameraPos = {
        "home": {
        "title": "Hem",
        camera: {"pos":{"h":960.2020999142313,"lat":65.85132035481274,"lng":23.1210609938601},"h":1.1578334969848907,"p":-0.9176593358807468,"r":0.0037926776574686016}
    },
    "bron": {
        "title": "Bron",
        camera: {
            pos: {
                h: 122.62584826600977,
                lat: 65.85006913153603,
                lng: 23.12636084926528
            },
            h: 0.56216647001379,
            p: -0.41910363560953945,
            r: 0.0014678630473357046,
        }
    },
    "galleria": {
        "title": "Galleria",
        camera: { "pos": { "h": 143.80370867873066, "lat": 65.85565951712087, "lng": 23.13493396138727 }, "h": 1.880459302443013, "p": -0.5237431777598043, "r": 0.0027624224889164495 }
    },

    "Church": {
        "title": "Kyrka",
        camera: { "pos": { "h": 30.933099795325862, "lat": 65.85406531346193, "lng": 23.1341300252016 }, "h": 4.6833588805886475, "p": -0.20982002158880442, "r": 6.280617242198932 }
    },

    "folketshus": {
        "title": "Folketshus",
        camera: {"pos":{"h":45.224866635263076,"lat":65.85644630090758,"lng":23.143211045954065},"h":1.1646836115592798,"p":-0.5853238812471409,"r":0.0027710824189028926}
    },


    "kiab": {
        "title": "Kiab Industrihus",
        "camera": {
            pos: {
                h: 97.1355321983106,
                lat: 65.84772118798492,
                lng: 23.200605144510696
            },
            h: 0.07857348207303083,
            p: -0.64836801774188,
            r: 0.000247974001014839
        }
    },
}

var data = {
    "bron": {
        "title": "Bron",
        "lat": 65.85380419,
        "lng": 23.13482645,
        "src": "models/parts/kalix_bridge.glb",
        camera: {
            pos: {
                h: 122.62584826600977,
                lat: 65.85006913153603,
                lng: 23.12636084926528
            },
            h: 0.56216647001379,
            p: -0.41910363560953945,
            r: 0.0014678630473357046,
        }
    },
    "roads": {
        "title": "Roads",
        "lat": 65.85380419,
        "lng": 23.13482645,
        "src": "models/parts/kalix_roads.glb",
        camera: {
            pos: {
                h: 122.62584826600977,
                lat: 65.85006913153603,
                lng: 23.12636084926528
            },
            h: 0.56216647001379,
            p: -0.41910363560953945,
            r: 0.0014678630473357046,
        }
    },

    "buildings": {
        "title": "Buildings",
        "lat": 65.85380419,
        "lng": 23.13482645,
        "src": "models/parts/kalix_buildings.glb",
        camera: {
            pos: {
                h: 122.62584826600977,
                lat: 65.85006913153603,
                lng: 23.12636084926528
            },
            h: 0.56216647001379,
            p: -0.41910363560953945,
            r: 0.0014678630473357046,
        }
    },

    "landmarks": {
        "title": "Landmarks",
        "lat": 65.85380419,
        "lng": 23.13482645,
        "src": "models/parts/kalix_landmarks.glb",
        camera: {
            pos: {
                h: 122.62584826600977,
                lat: 65.85006913153603,
                lng: 23.12636084926528
            },
            h: 0.56216647001379,
            p: -0.41910363560953945,
            r: 0.0014678630473357046,
        }
    },


    "kiab": {
        "title": "Kiab",
        "lng": 23.20017132,
        "lat": 65.84937730,
        "src": "models/kiab_lp.glb",
        "camera": {
            pos: {
                h: 97.1355321983106,
                lat: 65.84772118798492,
                lng: 23.200605144510696
            },
            h: 0.07857348207303083,
            p: -0.64836801774188,
            r: 0.000247974001014839
        }
    },
}
/*
    "kalix": {

        "title": "Kalix",
        "lat": 65.85380419,
        "lng": 23.13482645,
        "src": "models/parts/kalix_full01.glb"
    }

     "church": {
        "title": "Church",
        "lng": 23.131827,
        "lat": 65.853923,
        "src": "models/church.glb",
        "camera": {
			pos: {
				h: 120.21040047150862,
				lat: 65.85314209654209,
				lng: 23.13597603466481
			},
			h: 5.365336725777185,
			p: -0.5749791173098124,
			r: 6.280804523517444
		}
    },
*/
/* var height =0.3;
var heading = Math.PI / 180 * (90.0+2.1), pitch = 0.0, roll = 0.0;
var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll); */

Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZDIwYzM1MC1kYWNmLTQyMWQtYWFlNi02YWY2MjFjZDZkZmMiLCJpZCI6ODc5MSwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU1MjgyMzI3N30.FVbhAcjUughhOY044nMiv9gYLXf8pQI7kFnvGtjNPGE';

let viewer = new Cesium.Viewer('cesiumContainer', {
	scene3DOnly: true,
	selectionIndicator: false, // ?
	geocoder: false,
	homeButton: false,
	sceneModePicker: false,
	baseLayerPicker: false,
	navigationHelpButton: false,
	animation: false,
	timeline: false,
});

let camera = viewer.camera;
let scene = viewer.scene;
scene.globe.depthTestAgainstTerrain = false;
scene.primitives.removeAll();


var dataSource = Cesium.GeoJsonDataSource.load('../models/geojson/vag_wgs84.json').then(
	function (dataSource) {
		var p = dataSource.entities.values;
		for (var i = 0; i < p.length; i++) {
			p[i].polygon.extrudedHeight = 15; // or height property
		}
		viewer.dataSources.add(dataSource);
		viewer.zoomTo(dataSource);
	}
);




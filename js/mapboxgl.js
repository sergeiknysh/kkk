mapboxgl.accessToken = 'pk.eyJ1Ijoic2VyZ2Vpa255c2giLCJhIjoiY2swdmR4MnUzMHE5dzNjcGt0aHczMm43cSJ9.1aWjsboun1NDlSlwmDcYgg';

var placeCoords = new mapboxgl.LngLat(23.1985, 65.8492); // где все самое интересное

var gltfLoader = new THREE.GLTFLoader();
var map = new mapboxgl.Map({
	container: 'map',
	style: 'mapbox://styles/mapbox/light-v10',
	zoom: 17,
	center: [23, 65],
	pitch: 60,
	antialias: true // create the gl context with MSAA antialiasing, so custom layers are antialiased
});

map.on('style.load', function() {
	map.addLayer(threeLayer);
});

///////////////////
///////////////////
///////////////////

// parameters to ensure the model is georeferenced correctly on the map
var modelOrigin = [23.199615, 65.849166];
var modelAltitude = 0;
// transformation parameters to position, rotate and scale the 3D model onto the map
var modelPosition = mapboxgl.MercatorCoordinate.fromLngLat(modelOrigin, modelAltitude);
// Since our 3D model is in real world meters, a scale transform needs to be applied since the CustomLayerInterface expects units in MercatorCoordinates.
var modelScale = modelPosition.meterInMercatorCoordinateUnits(); // расстояние в 1м на данной широте в ед. измерения MercatorCoordinate
var modelRotation = new THREE.Matrix4().makeRotationX(Math.PI / 2); // какого-то хера gltf всегда какой-то повернутый
var projectionMatrixMultiplier = new THREE.Matrix4()
	.makeTranslation(modelPosition.x, modelPosition.y, modelPosition.z)
	.scale(new THREE.Vector3(modelScale, -modelScale, modelScale))
	.multiply(modelRotation)
	;
 
///////////////////
///////////////////
///////////////////

// configuration of the custom layer for a 3D model per the CustomLayerInterface
var threeLayer = {
	id: '3d-model',
	type: 'custom',
	renderingMode: '3d',
	
	camera: new THREE.Camera(),
	scene: new THREE.Scene(),

	onAdd: function(map, gl) {
		this.map = map;

		// use the Mapbox GL JS map canvas for three.js
		this.renderer = new THREE.WebGLRenderer({
			canvas: map.getCanvas(),
			context: gl,
			antialias: true,
		});
		 
		this.renderer.autoClear = false;
		this.renderer.gammaOutput = true;
	},

	render: function(gl, matrix) {
		// this.camera.projectionMatrix.elements = matrix; // вроде, не нужная фигня с учетом строки ниже
		this.camera.projectionMatrix = new THREE.Matrix4()
			.fromArray(matrix)
			.multiply(projectionMatrixMultiplier);

		this.renderer.state.reset(); // не понятно, что делает, но без него две модели рендеряться хреново
		this.renderer.render(this.scene, this.camera);
		// this.map.triggerRepaint(); // отключение перерисовки карты снижает потребление ресурсов в простое и, кажется, ни на что не влияет
	}
};

function addLights(layer) {
	var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 );
	hemiLight.position.set( 0, 50, 0 );
	threeLayer.scene.add( hemiLight );
	var hemiLightHelper = new THREE.HemisphereLightHelper( hemiLight, 10 );
	threeLayer.scene.add( hemiLightHelper );

	var dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
	dirLight.position.set( -2, 4, 2 );
	dirLight.position.multiplyScalar( 50 );		
	threeLayer.scene.add( dirLight );
	var dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 );
	threeLayer.scene.add( dirLightHeper );

	// lights from examples:
	// var directionalLight = (new THREE.DirectionalLight(0xffffff))
	// 	.position.set(0, -70, 100).normalize();
	// threeLayer.scene.add(directionalLight);
	// var directionalLight2 = (new THREE.DirectionalLight(0xffffff))
	// 	.position.set(0, 70, 100).normalize();
	// threeLayer.scene.add(directionalLight2);
}

function loadGLTFModel(layer, src) {
	gltfLoader.load(src, function(gltf) {
		var mesh = gltf.scene;
		layer.scene.add(mesh);
	});
}

function showCoords() {
	console.log({
		lat: map.getCenter().lat,
		lng: map.getCenter().lng,
		zoom: map.getZoom(),
		pitch: map.getPitch(),
		bearing: map.getBearing(),
	})
}

function gotoPlace() {
	map.flyTo({
		center: placeCoords,
		zoom: 17,
		pitch: 37.5,
		bearing: 9.63,
	})
}

/////////////////
/////////////////
/////////////////

addLights(threeLayer);

loadGLTFModel(threeLayer, 'models/kiab7.gltf');

// test second one
gltfLoader.load('models/kiab7.gltf', function(gltf) {
	var mesh = gltf.scene;
	mesh.translateZ(200);
	mesh.rotateY(Math.PI / 2);
	threeLayer.scene.add(mesh);
});

gotoPlace();

Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZDIwYzM1MC1kYWNmLTQyMWQtYWFlNi02YWY2MjFjZDZkZmMiLCJpZCI6ODc5MSwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU1MjgyMzI3N30.FVbhAcjUughhOY044nMiv9gYLXf8pQI7kFnvGtjNPGE';

var viewer = new Cesium.Viewer('cesiumContainer', {
    infoBox: false,
    selectionIndicator: false
});

var entity = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(-123, 44, 10),
    model: {
        uri: '../../models/CesiumTexturedBoxTest.gltf',
        minimumPixelSize: 128
    }
});
viewer.zoomTo(entity, new Cesium.HeadingPitchRange(Math.PI / 4, -Math.PI / 4, 3));


var image = new Image();
var texture;
image.src = '../images/checkerboard.png';
image.onload = function () {
    //scene.context.createTexture2D is not part of the public API,
    //so this code may have to change in the future.
/*     var texture = viewer.scene.context.createTexture2D({
        source: image
    }); */

    context = viewer.scene.context;
    
    texture = new Texture({
        context : context,
        source : image
    });

    //Change color on mouse over.  This relies on the fact that given a primitive,
    //you can retrieve an associted en
    var originalTexture;
    var lastPick;
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    var material;
    handler.setInputAction(function (movement) {
        var primitive;
        var pickedObject = viewer.scene.pick(movement.endPosition);
        if (pickedObject) {
            primitive = pickedObject.primitive;
            if (pickedObject !== lastPick && primitive instanceof Cesium.Model) {

                //We don't use the entity here, but if you need to color based on 
                //some entity property, you can get to that data it here.
                var entity = primitive.id;

                material = primitive.getMaterial('Texture');
                originalTexture = material.getValue('diffuse');
                material.setValue('diffuse', texture);
                lastPick = pickedObject;
            }
        } else if (lastPick) {
            primitive = lastPick.primitive;
            material = primitive.getMaterial('Texture');
            material.setValue('diffuse', originalTexture);
            lastPick = undefined;
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
};


/* Cesium.loadImage('../images/checkerboard.png').then(function (image) {
    //scene.context.createTexture2D is not part of the public API,
    //so this code may have to change in the future.
    var texture = viewer.scene.context.createTexture2D({
        source: image
    });

    //Change color on mouse over.  This relies on the fact that given a primitive,
    //you can retrieve an associted en
    var originalTexture;
    var lastPick;
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    var material;
    handler.setInputAction(function (movement) {
        var primitive;
        var pickedObject = viewer.scene.pick(movement.endPosition);
        if (pickedObject) {
            primitive = pickedObject.primitive;
            if (pickedObject !== lastPick && primitive instanceof Cesium.Model) {

                //We don't use the entity here, but if you need to color based on 
                //some entity property, you can get to that data it here.
                var entity = primitive.id;

                material = primitive.getMaterial('Texture');
                originalTexture = material.getValue('diffuse');
                material.setValue('diffuse', texture);
                lastPick = pickedObject;
            }
        } else if (lastPick) {
            primitive = lastPick.primitive;
            material = primitive.getMaterial('Texture');
            material.setValue('diffuse', originalTexture);
            lastPick = undefined;
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}); */

//Ability to look up the Model associated with an entity
function getModelForEntity(entity) {
    var primitives = viewer.scene.primitives;
    for (var i = 0; i < primitives.length; i++) {
        var primitive = primitives.get(i);
        if (primitive instanceof Cesium.Model && primitive.id === entity) {
            return primitive;
        }
    }
}